﻿using System;
using SimpleUnitTestsAndFakes.Contracts;
using SimpleUnitTestsAndFakes.Contracts.External;
using SimpleUnitTestsAndFakes.Entities;

namespace SimpleUnitTestsAndFakes
{
    public class Calculator : ICalculator
    {
        private IHrService hrService;
        private IMailService mailService;

        public Calculator(IHrService hrService, IMailService mailService)
        {
            this.hrService = hrService;
            this.mailService = mailService;
        }

        public int CalculateSalary(Person person)
        {
            if (person.Age < 18 || person.Age > 70)
            {
                throw new ArgumentException();
            }

            var salary = (int)(hrService.GetSkillRate() * person.Skill
                               + hrService.GetAgeRate() * person.Age);

            mailService.Send(person.EmailAddress, salary.ToString());

            return salary;
        }
    }
}