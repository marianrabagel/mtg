﻿namespace SimpleUnitTestsAndFakes.Contracts.External
{
    public interface IMailService
    {
        void Send(string emailAddress, string content);
    }
}
