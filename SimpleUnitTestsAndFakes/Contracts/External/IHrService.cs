﻿namespace SimpleUnitTestsAndFakes.Contracts.External
{
    public interface IHrService
    {
        decimal GetSkillRate();
        decimal GetAgeRate();
    }
}
