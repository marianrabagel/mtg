﻿
using SimpleUnitTestsAndFakes.Entities;

namespace SimpleUnitTestsAndFakes.Contracts
{
    public interface ICalculator
    {
        int CalculateSalary(Person person);
    }
}
