﻿namespace SimpleUnitTestsAndFakes.Contracts
{
    public interface IEmailer
    {
        void Send(string emailAddress, int salary);
    }
}
