﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleUnitTestsAndFakes.Contracts;
using SimpleUnitTestsAndFakes.Contracts.External;
using SimpleUnitTestsAndFakes.Entities;

namespace SimpleUnitTestsAndFakes.UnitTests
{
    [TestClass]
    public class CalculatorTests
    {
        private IHrService fakeHrService;
        private IMailService fakeMailService;
        private ICalculator calculator;

        [TestInitialize]
        public void Setup()
        {
            fakeHrService = new FakeHrService();
            fakeMailService = new FakeMailService();

            calculator = new Calculator(fakeHrService, fakeMailService);
        }

        [TestMethod]
        public void TestThatWhenAgeIs20AndSkillIs10CalculateSalaryReturns1200()
        {
            var person = new Person
            {
                Age = 20,
                Skill = 10
            };
            
            var result = calculator.CalculateSalary(person);

            Assert.AreEqual(1200, result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestThatWhenAgeIs15AndSkillIs7CalculateSalaryReturns850()
        {
            var person = new Person
            {
                Age = 15,
                Skill = 7
            };
             
            var result = calculator.CalculateSalary(person);

            Assert.AreEqual(850, result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestThatWhenAgeIs74AndSkillIs4CalculateSalaryReturns1140()
        {
            var person = new Person
            {
                Age = 74,
                Skill = 4
            };
             
            var result = calculator.CalculateSalary(person);

            Assert.AreEqual(1140, result);
        }

        [TestMethod]
        public void TestThatCalculateSalaryCallsSendEmail()
        {
            var person = new Person
            {
                Age = 30,
                Skill = 10
            };
             
            calculator.CalculateSalary(person);

            Assert.IsTrue(((FakeMailService) fakeMailService).HasBeenCalled);
        }
    }

    internal class FakeMailService : IMailService
    {
        public bool HasBeenCalled;

        public void Send(string emailAddress, string content)
        {
            HasBeenCalled = true;
        }
    }

    internal  class FakeHrService : IHrService
    {
        public decimal GetSkillRate()
        {
            return 100;
        }

        public decimal GetAgeRate()
        {
            return 10;
        }
    }
}
