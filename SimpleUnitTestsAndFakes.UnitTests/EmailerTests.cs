﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleUnitTestsAndFakes.Contracts;
using SimpleUnitTestsAndFakes.Contracts.External;

namespace SimpleUnitTestsAndFakes.UnitTests
{
    [TestClass]
    public class EmailerTests
    {
        private IMailService fakeMailService;
        private IEmailer emailer;

        [TestInitialize]
        public void Setup()
        {
            fakeMailService = new FakeMailService();
            emailer = new Emailer(fakeMailService);
        }

        [TestMethod]
        public void TestThat()
        {
            var emailAddress = "";
            int salary = 1000;

            emailer.Send(emailAddress, salary);
        }
    }
}
